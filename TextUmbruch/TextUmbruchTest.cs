﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TextUmbruch
{
    using System;
    using System.Collections.Generic;

    [TestClass]
    public class TextUmbruchTest
    {
        [TestMethod]
        public void TestUmbruchSimple()
        {
            const string input = "a\nb\nc";
            var output = TextUmbruch.Umbrechen(input, 1);
            Assert.AreEqual(input, output);
        }

        [TestMethod]
        public void TestUmbruchMehrereWoerterProZeile()
        {
            const string input = "a b c";
            var output = TextUmbruch.Umbrechen(input, 3);

            var expected = "a b\nc";
            Assert.AreEqual(expected, output);
        }

        [TestMethod]
        public void TestUmbruchMehrteiligeWoerter14()
        {
            const string input = "Es blaut die Nacht,\ndie Sternlein blinken,\nSchneeflöcklein leis hernieder sinken.";
            var output = TextUmbruch.Umbrechen(input, 14);

            var expected = "Es blaut die\nNacht, die\nSternlein\nblinken,\nSchneeflöcklei\nn leis\nhernieder\nsinken.";
            Assert.AreEqual(expected, output);
        }

        [TestMethod]
        public void TestUmbruchMehrteiligeWoerter9()
        {
            const string input = "Es blaut die Nacht,\ndie Sternlein blinken,\nSchneeflöcklein leis hernieder sinken.";
            var output = TextUmbruch.Umbrechen(input, 9);

            var expected = "Es blaut\ndie\nNacht,\ndie\nSternlein\nblinken,\nSchneeflö\ncklein\nleis\nhernieder\nsinken.";
            Assert.AreEqual(expected, output);
        }

        [TestMethod]
        public void TestUmbruchZuLangeWorteTrennen()
        {
            var worte = new string[] {"a", "bcd", "efghi" };
            var output = TextUmbruch.ZuLangeWorteTrennen(worte, 2);

            var expectedWorte = new string[] { "a", "bc", "d", "ef", "gh", "i" };                //"Es blaut die\nNacht, die\nSternlein\nblinken,\nSchneeflöcklei\nn leis\nhernieder\nsinken.";
            CollectionAssert.AreEqual(expectedWorte, output);
        }

        public void TestUmruchWortUmbrechen()
        {
            const string input = "line1 l ine2";
            var output = TextUmbruch.Umbrechen(input, 7);

            var expected = "line1 l\nine2";
            Assert.AreEqual(expected, output);

        }
    }
}
