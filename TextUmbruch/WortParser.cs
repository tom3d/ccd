namespace TextUmbruch
{
    using System;

    internal class WortParser
    {
        public static string[] WoerterBilden(string text)
        {
            return text.Split(new[] {' ', '\n'}, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}