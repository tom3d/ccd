﻿namespace TextUmbruch
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class TestWortParser
    {
        [TestMethod]
        public void TestWoerterBilden_Simple()
        {
            const string input = "a b c";
            var expected = new[] { "a", "b", "c" };
            var actual = WortParser.WoerterBilden(input);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestWoerterBilden_DoubleSpace()
        {
            const string input = "a  b c  ";
            var expected = new[] { "a", "b", "c" };
            var actual = WortParser.WoerterBilden(input);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestWoerterBilden_Commas()
        {
            const string input = "a ,b c,";
            var expected = new[] { "a", ",b", "c," };
            var actual = WortParser.WoerterBilden(input);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestWoerterBilden_CommasAsWords()
        {
            const string input = "a , b c,";
            var expected = new[] { "a", ",", "b", "c," };
            var actual = WortParser.WoerterBilden(input);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestWoerterBilden_Newlines()
        {
            const string input = "\na\n\nb\n";
            var expected = new[] { "a", "b" };
            var actual = WortParser.WoerterBilden(input);
            CollectionAssert.AreEqual(expected, actual);
        }


    }
}
