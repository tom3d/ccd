namespace TextUmbruch
{
    using System.Collections.Generic;
    using System.Linq;

    public class TextUmbruch
    {
        public static string Umbrechen(string text, int maxZeilenLaenge)
        {
            string[] wortliste = TextInWoerterUmwandeln(text, maxZeilenLaenge);
            var zeilen = ZeilenBauen(wortliste, maxZeilenLaenge);
            var ausgabe = AusgabeTextAufbereiten(zeilen);
            return ausgabe;
        }

        private static string[] TextInWoerterUmwandeln(string text, int maxZeilenLaenge)
        {
            var wortliste = WortParser.WoerterBilden(text);
            wortliste = ZuLangeWorteTrennen(wortliste, maxZeilenLaenge);
            return wortliste;
        }

        private static string[] ZeilenBauen(string[] wortliste, int maxZeilenLaenge)
        {
            var wortgruppen = WorteZusammenfassenProZeile(wortliste.ToList(), maxZeilenLaenge);
            return AusAllenWortgruppenZeilenBauen(wortgruppen);
        }

        private static string AusgabeTextAufbereiten(string[] input)
        {
            return string.Join("\n", input);
        }


        private static string[] WorteDerZeileBestimmen(List<string> wortliste, int maxZeilenLaenge)
        {
            const int wordSeparatorLength = 1;
            List<string> wortgruppe = new List<string>();
            int currentLineLength = 0;
            while (wortliste.Any())
            {
                string currentWord = wortliste[0];

                int newLineLength = currentLineLength + currentWord.Length;
                bool inTheMiddleOfTheLine = currentLineLength > 0;
                if (inTheMiddleOfTheLine)
                {
                    newLineLength += wordSeparatorLength;
                }

                bool lineCompleted = newLineLength > maxZeilenLaenge;
                if (lineCompleted)
                {
                    break;
                }

                wortgruppe.Add(currentWord);
                wortliste.RemoveAt(0);
                currentLineLength = newLineLength;
            }
            return wortgruppe.ToArray();
        }

        private static string[][] WorteZusammenfassenProZeile(List<string> wortliste, int maxZeilenLaenge)
        {
            List<string[]> wortgruppen= new List<string[]>();

            while (wortliste.Any())
            {
                string[] wortgruppe = WorteDerZeileBestimmen(wortliste, maxZeilenLaenge);
                wortgruppen.Add(wortgruppe);

            }
            return wortgruppen.ToArray();
        }

        private static string[] AusAllenWortgruppenZeilenBauen(string[][] wortgruppen)
        {
            return wortgruppen.Select(wortgruppe => string.Join(" ", wortgruppe)).ToArray();
        }

        public static string[] ZuLangeWorteTrennen(string[] worte, int maxZeilenLaenge)
        {
            return worte.SelectMany(w => WortTrennen(w, maxZeilenLaenge)).ToArray();
        }

        private static string[] WortTrennen(string wort, int maxZeilenLaenge)
        {
            var silben = new List<string>();
            do
            {
                string silbe = new string(wort.Take(maxZeilenLaenge).ToArray());
                silben.Add(silbe);

                wort = new string(wort.Skip(maxZeilenLaenge).ToArray());
            } while (wort.Length != 0);

            return silben.ToArray();
        }
    }
}
